# imagen de node a usar
FROM node:21.7.2 AS builder

# carpeta de trabajo
WORKDIR /app

# copia todos los package*.json y los pega en el dokcer
COPY package*.json ./

# instala las dependecias
RUN npm i

# copia todo el proyecto y lo pega en el docker
COPY . .

# compila la app(crea la carpeta dist con la app hasheada)
RUN npm run build

# imagen de nginx a usar
FROM nginx:latest

# copio my vhost personalizado y lo pego en el vhost de nginx, al del docker
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

# copio my app compilada y la pega en el document root que puse en el default.conf
COPY --from=builder /app/dist /usr/share/nginx/html

# puerto que usa docker internamente
EXPOSE 80

# inicia el nginx
CMD ["nginx", "-g", "daemon off;"]